<?php

namespace WebForceHQ\Transformers;

class BrandTransformer extends Transformer{

	public function transform($item)
    {
        return [
            'id' => $item['id'],
            'name' => $item['name']
        ];
    }
}
