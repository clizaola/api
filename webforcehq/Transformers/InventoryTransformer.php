<?php

namespace WebForceHQ\Transformers;

class InventoryTransformer extends Transformer{

	public function transform($item)
    {
        return [
            //'id' => $item['id'],
            //'store' => $item['store']['name'],
            'product' => $item['product']['name'],
            'quantity' => $item['quantity']
        ];
    }
}
