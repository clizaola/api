<?php

namespace WebForceHQ\Transformers;

class StoreTransformer extends Transformer{

	public function transform($item)
    {
        return [
            'id' => $item['id'],
            'name' => $item['name'],
            'address' => $item['address'],
            'city' => $item['city'],
            'state' => $item['state'],
            'zip_code' => $item['zip_code']
        ];
    }
}
