<?php

namespace WebForceHQ\Transformers;

class ProductTransformer extends Transformer{

	public function transform($item)
    {
        return [
            'id' => $item['id'],
            'name' => $item['name'],
            'brand' => $item['brand']['name']
        ];
    }
}
