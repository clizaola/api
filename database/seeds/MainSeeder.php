<?php

use App\Brand;
use App\Store;
use App\Product;
use App\Inventory;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
class MainSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Faker library
         * @var Faker
         */
        $fake = Faker::create();
        
        /**
         * Brands list
         * @var Array
         */
        $brands = \File::getRequire(base_path().'/database/seeds/brands.php');

        /**
         * Products list
         * @var Array
         */
        $products = \File::getRequire(base_path().'/database/seeds/products.php');

        /**
         * Stores list
         * @var array
         */
        $stores = \File::getRequire(base_path().'/database/seeds/stores.php');



        foreach ($brands as $brand) {
            Brand::create(['name'=>$brand]);
        }

        
        // Get the id for all the brands and export to an array
        $brands = Brand::all()->pluck('id')->all();

        foreach ($products as $product) {
            Product::create([
                'name' => $product,
                'brand_id' => $fake->randomElement($brands)
            ]);
        }

        foreach ($stores as $store) {
            Store::create([
                'name' => $store,
                'address' => $fake->streetAddress,
                'city' => $fake->city,
                'state' => $fake->stateAbbr,
                'zip_code' => $fake->postcode
            ]);
        }

        // Get the id for all the products, and stores
        $products = Product::all()->pluck('id')->all();
        $stores = Store::all()->pluck('id')->all();

        foreach ($stores as $storeId) {
            foreach (range(1, mt_rand(1,15)) as $index) {
                Inventory::create([
                    'store_id' => $storeId,
                    'product_id' => $fake->randomElement($products),
                    'quantity' => mt_rand(1,30)
                ]);
            }
            
        }

    }
}









