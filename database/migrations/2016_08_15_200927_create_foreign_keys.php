<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    public $foreignKeys = [
        'inventories' => [
            'products'  => 'product_id',
            'stores'    => 'store_id'
        ],
        'products'    => [
            'brands'    => 'brand_id'
        ]
    ];


    public function up()
    {
        foreach ($this->foreignKeys as $tableName => $foreignKeys) {
            foreach ($foreignKeys as $foreignTableName => $keyName) {
                Schema::table($tableName, function (Blueprint $table) use ($foreignTableName, $keyName) {
                    $table->foreign($keyName)->references('id')->on($foreignTableName)->onUpdate('no action')->onDelete('cascade');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $dropForeignKeys = array_reverse($this->foreignKeys);

        foreach ($dropForeignKeys as $tableName => $foreignKeys) {
            foreach ($foreignKeys as $keyName) {
                Schema::table($tableName, function (Blueprint $table) use ($tableName, $keyName) {
                    $table->dropForeign($tableName.'_'.$keyName.'_foreign');
                });
            }
        }
    }
}
