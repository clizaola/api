<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Brands end points
Route::get('/brands', 'Api\BrandsController@index');
Route::get('/brands/{id}', 'Api\BrandsController@show');
Route::post('/brands', 'Api\BrandsController@store');

// Products end points
Route::get('/products', 'Api\ProductsController@index');
Route::get('/products/{id}', 'Api\ProductsController@show');
Route::post('/products', 'Api\ProductsController@store');

// Stores end points
Route::get('/stores', 'Api\StoresController@index');
Route::get('/stores/{id}', 'Api\StoresController@show');

Route::get('/stores/{id}/inventories', 'Api\StoreInventoriesController@index');


