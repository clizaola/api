<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('soa', function(){
	error_reporting(E_ALL);
	require base_path('vendor/novaksolutions/infusionsoft-php-sdk/Infusionsoft/infusionsoft.php');

	$request = request();

	$returnFields = array('Id', 'Email', 'FirstName', 'LastName');
	$tagId = 622;
	$email = $request->email;
	$firstName = $request->first_name;
	$lastName = $request->last_name;
	$redirectTo = $request->redirect_to;

	$contact = new Infusionsoft_Contact();
	$contact->FirstName = $firstName;
	$contact->LastName = $lastName;
	$contact->Email = $email;
	$contact->save();
	$result = Infusionsoft_EmailService::optIn($email,'A Shot Of Adrenaline');
	
	$contact = Infusionsoft_ContactService::findByEmail($email,$returnFields);
	$contact = $contact[0];

	Infusionsoft_ContactService::addToGroup($contact['Id'],$tagId);

	header("Location: {$redirectTo}");
	exit;
});

