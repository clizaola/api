<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','brand_id'];

    /**
     * @return Eloquent Relation
     */
    public function brand(){
    	return $this->belongsTo('App\Brand');
    }
}
