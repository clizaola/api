<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use WebForceHQ\Transformers\ProductTransformer;

class ProductsController extends ApiController
{
    /**
     * [$productTransformer description]
     * @var [ProductTransformer]
     */
    protected $productTransformer;

    /**
     * @param ProductTransformer
     */
    public function __construct(ProductTransformer $productTransformer)
    {
        $this->productTransformer = $productTransformer;
    }

    /**
     * @return Response
     */
    public function index()
    {
        $products = Product::all();

        return $this->respond([
            'data' => $this->productTransformer->transformCollection($products->all())
        ]);
    }

    /**
     * @param  integer
     * @return Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if (! $product) {
            return $this->respondNotFound('Product not found');
        }

        return $this->respond([
            'data' => $this->productTransformer->transform($product)
        ]);
    }

    /**
     * @return [type]
     */
    public function store(Request $request)
    {
        if (! $request->name || $request->brand_id ) {
        	return $this->respondUnprocessable('Faild Validation');
        }

        $product = Product::create($request->all());

        return $this->respondCreated('Product Successfully Created', $this->productTransformer->transform($product));
    }
}
