<?php

namespace App\Http\Controllers\Api;

use App\Brand;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use WebForceHQ\Transformers\BrandTransformer;

class BrandsController extends ApiController
{
    /**
     * [$brandTransformer description]
     * @var [BrandTransformer]
     */
    protected $brandTransformer;

    /**
     * @param BrandTransformer
     */
    public function __construct(BrandTransformer $brandTransformer)
    {
        $this->brandTransformer = $brandTransformer;
    }

    /**
     * @return Response
     */
    public function index()
    {
        $brands = Brand::all();

        return $this->respond([
            'data' => $this->brandTransformer->transformCollection($brands->all())
        ]);
    }

    /**
     * @param  integer
     * @return Response
     */
    public function show($id)
    {
        $brand = Brand::find($id);

        if (! $brand) {
            return $this->respondNotFound('Brand not found');
        }

        return $this->respond([
            'data' => $this->brandTransformer->transform($brand)
        ]);
    }

    /**
     * @return [type]
     */
    public function store(Request $request)
    {
        if (! $request->name) {
        	return $this->respondUnprocessable('Faild Validation');
        }

        $brand = Brand::create($request->all());

        return $this->respondCreated('Successfully Created', $this->brandTransformer->transform($brand));
    }
}
