<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * [$statusCode description]
     * @var integer
     */
    protected $statusCode = 200;

    /**
     * @return integer
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param integer
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param  string
     * @return Response
     */
    public function respondNotFound($message='Not Found!')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * @param  string
     * @return Response
     */
    public function respondInternalError($message='Internal Error!')
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }

    /**
     * @param  string
     * @return Response
     */
    public function respondUnprocessable($message='Unprocessable Entity')
    {
    	return $this->setStatusCode(422)->respondWithError($message);
    }

    /**
     * @param  array
     * @param  array
     * @return type Response
     */
    public function respond($data, $headers=[])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /**
     * @param  string
     * @return Response
     */
    public function respondWithError($message)
    {
    	return $this->respond([
            'error' => $message,
            'status_code' => $this->getStatusCode()
        ]);
    }

    /**
     * @param  string
     * @param  Object
     * @return Response
     */
    public function respondCreated($message='Successfully Created', $data=null)
    {
    	return $this->setStatusCode(201)->respond([
        	'message' => $message,
        	'data' => $data
        ]);
    }

    
}

