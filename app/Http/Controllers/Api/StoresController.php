<?php

namespace App\Http\Controllers\Api;

use App\Store;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use WebForceHQ\Transformers\StoreTransformer;

class StoresController extends ApiController
{
    /**
     * [$storeTransformer description]
     * @var [StoreTransformer]
     */
    protected $storeTransformer;

    /**
     * @param StoreTransformer
     */
    public function __construct(StoreTransformer $storeTransformer)
    {
        $this->storeTransformer = $storeTransformer;
    }

    /**
     * @return Response
     */
    public function index()
    {
        $stores = Store::all();

        return $this->respond([
            'data' => $this->storeTransformer->transformCollection($stores->all())
        ]);
    }

    /**
     * @param  integer
     * @return Response
     */
    public function show($id)
    {
        $store = Store::find($id);

        if (! $store) {
            return $this->respondNotFound('Store not found');
        }

        return $this->respond([
            'data' => $this->storeTransformer->transform($store)
        ]);
    }

    /**
     * @return [type]
     */
    public function store(Request $request)
    {
        if (! $request->name) {
        	return $this->respondUnprocessable('Faild Validation');
        }

        $store = Store::create($request->all());

        return $this->respondCreated('Successfully Created', $this->storeTransformer->transform($store));
    }
}
