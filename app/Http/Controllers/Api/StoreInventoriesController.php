<?php

namespace App\Http\Controllers\Api;

use App\Inventory;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use WebForceHQ\Transformers\InventoryTransformer;

class StoreInventoriesController extends ApiController
{
    /**
     * [$inventoryTransformer description]
     * @var [InventoryTransformer]
     */
    protected $inventoryTransformer;

    /**
     * @param InventoryTransfoyr
     */
    public function __construct(InventoryTransformer $inventoryTransformer)
    {
        $this->inventoryTransformer = $inventoryTransformer;
    }

    /**
     * @return Response
     */
    public function index($storeId)
    {
        $inventories = Inventory::whereStoreId($storeId)->get();


        if ($inventories->isEmpty()) {
        	return $this->respondNotFound('Not found');
        }

        return $this->respond([
            'data' => $this->inventoryTransformer->transformCollection($inventories->all())
        ]);
    }
}
