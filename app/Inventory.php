<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable = ['store_id','product_id','quantity'];

    public function store()
    {
    	return $this->belongsTo('App\Store');
    }

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }
}
