@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    Your api token is: <br />
                    <div style="width: 100%" class="text-center">
                    <b>{{ auth()->user()->api_token }}</b><br /><br />
                    </div>
                    Try this:<br />
                    <h3>GET - /api/brands</h3>
                    <a href="{{ $domain }}/api/brands?api_token={{ $token }}" target="_blank">
                        {{ $domain }}/api/brands?api_token={{ $token }}
                    </a>

                    <h3>GET - /api/products</h3>
                    <a href="{{ $domain }}/api/products?api_token={{ $token }}" target="_blank">
                        {{ $domain }}/api/products?api_token={{ $token }}
                    </a>

                    <h3>GET - /api/stores</h3>
                    <a href="{{ $domain }}/api/stores?api_token={{ $token }}" target="_blank">
                        {{ $domain }}/api/stores?api_token={{ $token }}
                    </a>

                    <h3>GET - /api/stores/{storeId}/inventories</h3>
                    <a href="{{ $domain }}/api/stores/1/inventories?api_token={{ $token }}" target="_blank">
                        {{ $domain }}/api/stores/1/inventories?api_token={{ $token }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
